const axios = require('axios')
const btoa = require('btoa')
// Import and setup  B2 service
const accountId = process.env.B2_ACCOUNT_ID
const applicationKey = process.env.B2_APPLICATION_KEY
const b2Authentication = btoa(`${accountId}:${applicationKey}`)

const authorizeB2 = () => axios({
  url: 'https://api.backblazeb2.com/b2api/v2/b2_authorize_account',
  method: 'post',
  headers: { Authorization: `Basic ${b2Authentication}` },
  data: {}
}).then(({ data }) => data).catch(({ response: { data: err } }) => { throw new Error(err) })

const b2Functions = {
  async getUploadUrl () {
    // Authorize B2
    const { apiUrl, authorizationToken } = await authorizeB2()
    // Get Upload URL
    return axios({
      method: 'post',
      url: `${apiUrl}/b2api/v2/b2_get_upload_url`,
      headers: { Authorization: authorizationToken },
      data: { bucketId: process.env.B2_BUCKET }
    }).then(({ data }) => data).catch(({ response: { data: err } }) => {
      console.log(err)
      throw new Error(err)
    })
  },
  async getDownloadAuth (userId) {
    const { apiUrl, downloadUrl, authorizationToken } = await authorizeB2()
    const downloadAuthorization = await axios({
      method: 'post',
      url: `${apiUrl}/b2api/v2/b2_get_download_authorization`,
      headers: { Authorization: authorizationToken },
      data: {
        bucketId: process.env.B2_BUCKET,
        fileNamePrefix: userId,
        validDurationInSeconds: 600
      }
    }).then(({ data }) => data).catch(({ response: { data: err } }) => {
      console.log(err)
      throw new Error(err)
    })
    return {
      downloadUrl,
      downloadAuthorization
    }
  }
}

module.exports = b2Functions
