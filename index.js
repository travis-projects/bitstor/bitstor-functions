'use strict'

const { ApolloServer } = require('apollo-server-cloud-functions')
const jwt = require('jsonwebtoken')

const resolvers = require('./resolvers')
const typeDefs = require('./typedefs')

function getUserId ({ headers }) {
  const token = headers.authorization
  if (token) {
    const { userId } = jwt.verify(token, process.env.APP_SECRET)
    return userId
  }
  throw new AuthError()
}

class AuthError extends Error {
  constructor () {
    super('Not authorized')
  }
}

const server = new ApolloServer({
  typeDefs,
  resolvers,
  context: ({ req, res }) => ({
    headers: req.headers,
    getUserId,
    req,
    res
  }),
  playground: false,
  introspection: true
})

exports.graphql = server.createHandler({ cors: {
  origin: '*',
  credentials: true
} })
