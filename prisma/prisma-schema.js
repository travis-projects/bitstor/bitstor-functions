module.exports = {
  typeDefs: /* GraphQL */ `type AggregateBit {
  count: Int!
}

type AggregateSpace {
  count: Int!
}

type AggregateUser {
  count: Int!
}

type BatchPayload {
  count: Long!
}

type Bit {
  id: ID!
  owner: User!
  name: String
  file: String
  version: Float
}

type BitConnection {
  pageInfo: PageInfo!
  edges: [BitEdge]!
  aggregate: AggregateBit!
}

input BitCreateInput {
  owner: UserCreateOneInput!
  name: String
  file: String
  version: Float
}

input BitCreateManyInput {
  create: [BitCreateInput!]
  connect: [BitWhereUniqueInput!]
}

type BitEdge {
  node: Bit!
  cursor: String!
}

enum BitOrderByInput {
  id_ASC
  id_DESC
  name_ASC
  name_DESC
  file_ASC
  file_DESC
  version_ASC
  version_DESC
  createdAt_ASC
  createdAt_DESC
  updatedAt_ASC
  updatedAt_DESC
}

type BitPreviousValues {
  id: ID!
  name: String
  file: String
  version: Float
}

type BitSubscriptionPayload {
  mutation: MutationType!
  node: Bit
  updatedFields: [String!]
  previousValues: BitPreviousValues
}

input BitSubscriptionWhereInput {
  mutation_in: [MutationType!]
  updatedFields_contains: String
  updatedFields_contains_every: [String!]
  updatedFields_contains_some: [String!]
  node: BitWhereInput
  AND: [BitSubscriptionWhereInput!]
  OR: [BitSubscriptionWhereInput!]
  NOT: [BitSubscriptionWhereInput!]
}

input BitUpdateDataInput {
  owner: UserUpdateOneRequiredInput
  name: String
  file: String
  version: Float
}

input BitUpdateInput {
  owner: UserUpdateOneRequiredInput
  name: String
  file: String
  version: Float
}

input BitUpdateManyInput {
  create: [BitCreateInput!]
  update: [BitUpdateWithWhereUniqueNestedInput!]
  upsert: [BitUpsertWithWhereUniqueNestedInput!]
  delete: [BitWhereUniqueInput!]
  connect: [BitWhereUniqueInput!]
  disconnect: [BitWhereUniqueInput!]
}

input BitUpdateWithWhereUniqueNestedInput {
  where: BitWhereUniqueInput!
  data: BitUpdateDataInput!
}

input BitUpsertWithWhereUniqueNestedInput {
  where: BitWhereUniqueInput!
  update: BitUpdateDataInput!
  create: BitCreateInput!
}

input BitWhereInput {
  id: ID
  id_not: ID
  id_in: [ID!]
  id_not_in: [ID!]
  id_lt: ID
  id_lte: ID
  id_gt: ID
  id_gte: ID
  id_contains: ID
  id_not_contains: ID
  id_starts_with: ID
  id_not_starts_with: ID
  id_ends_with: ID
  id_not_ends_with: ID
  owner: UserWhereInput
  name: String
  name_not: String
  name_in: [String!]
  name_not_in: [String!]
  name_lt: String
  name_lte: String
  name_gt: String
  name_gte: String
  name_contains: String
  name_not_contains: String
  name_starts_with: String
  name_not_starts_with: String
  name_ends_with: String
  name_not_ends_with: String
  file: String
  file_not: String
  file_in: [String!]
  file_not_in: [String!]
  file_lt: String
  file_lte: String
  file_gt: String
  file_gte: String
  file_contains: String
  file_not_contains: String
  file_starts_with: String
  file_not_starts_with: String
  file_ends_with: String
  file_not_ends_with: String
  version: Float
  version_not: Float
  version_in: [Float!]
  version_not_in: [Float!]
  version_lt: Float
  version_lte: Float
  version_gt: Float
  version_gte: Float
  AND: [BitWhereInput!]
  OR: [BitWhereInput!]
  NOT: [BitWhereInput!]
}

input BitWhereUniqueInput {
  id: ID
}

scalar Long

type Mutation {
  createBit(data: BitCreateInput!): Bit!
  updateBit(data: BitUpdateInput!, where: BitWhereUniqueInput!): Bit
  updateManyBits(data: BitUpdateInput!, where: BitWhereInput): BatchPayload!
  upsertBit(where: BitWhereUniqueInput!, create: BitCreateInput!, update: BitUpdateInput!): Bit!
  deleteBit(where: BitWhereUniqueInput!): Bit
  deleteManyBits(where: BitWhereInput): BatchPayload!
  createSpace(data: SpaceCreateInput!): Space!
  updateSpace(data: SpaceUpdateInput!, where: SpaceWhereUniqueInput!): Space
  updateManySpaces(data: SpaceUpdateInput!, where: SpaceWhereInput): BatchPayload!
  upsertSpace(where: SpaceWhereUniqueInput!, create: SpaceCreateInput!, update: SpaceUpdateInput!): Space!
  deleteSpace(where: SpaceWhereUniqueInput!): Space
  deleteManySpaces(where: SpaceWhereInput): BatchPayload!
  createUser(data: UserCreateInput!): User!
  updateUser(data: UserUpdateInput!, where: UserWhereUniqueInput!): User
  updateManyUsers(data: UserUpdateInput!, where: UserWhereInput): BatchPayload!
  upsertUser(where: UserWhereUniqueInput!, create: UserCreateInput!, update: UserUpdateInput!): User!
  deleteUser(where: UserWhereUniqueInput!): User
  deleteManyUsers(where: UserWhereInput): BatchPayload!
}

enum MutationType {
  CREATED
  UPDATED
  DELETED
}

interface Node {
  id: ID!
}

type PageInfo {
  hasNextPage: Boolean!
  hasPreviousPage: Boolean!
  startCursor: String
  endCursor: String
}

type Query {
  bit(where: BitWhereUniqueInput!): Bit
  bits(where: BitWhereInput, orderBy: BitOrderByInput, skip: Int, after: String, before: String, first: Int, last: Int): [Bit]!
  bitsConnection(where: BitWhereInput, orderBy: BitOrderByInput, skip: Int, after: String, before: String, first: Int, last: Int): BitConnection!
  space(where: SpaceWhereUniqueInput!): Space
  spaces(where: SpaceWhereInput, orderBy: SpaceOrderByInput, skip: Int, after: String, before: String, first: Int, last: Int): [Space]!
  spacesConnection(where: SpaceWhereInput, orderBy: SpaceOrderByInput, skip: Int, after: String, before: String, first: Int, last: Int): SpaceConnection!
  user(where: UserWhereUniqueInput!): User
  users(where: UserWhereInput, orderBy: UserOrderByInput, skip: Int, after: String, before: String, first: Int, last: Int): [User]!
  usersConnection(where: UserWhereInput, orderBy: UserOrderByInput, skip: Int, after: String, before: String, first: Int, last: Int): UserConnection!
  node(id: ID!): Node
}

type Space {
  id: ID!
  owner: User!
  name: String
  description: String
  bits(where: BitWhereInput, orderBy: BitOrderByInput, skip: Int, after: String, before: String, first: Int, last: Int): [Bit!]
}

type SpaceConnection {
  pageInfo: PageInfo!
  edges: [SpaceEdge]!
  aggregate: AggregateSpace!
}

input SpaceCreateInput {
  owner: UserCreateOneInput!
  name: String
  description: String
  bits: BitCreateManyInput
}

type SpaceEdge {
  node: Space!
  cursor: String!
}

enum SpaceOrderByInput {
  id_ASC
  id_DESC
  name_ASC
  name_DESC
  description_ASC
  description_DESC
  createdAt_ASC
  createdAt_DESC
  updatedAt_ASC
  updatedAt_DESC
}

type SpacePreviousValues {
  id: ID!
  name: String
  description: String
}

type SpaceSubscriptionPayload {
  mutation: MutationType!
  node: Space
  updatedFields: [String!]
  previousValues: SpacePreviousValues
}

input SpaceSubscriptionWhereInput {
  mutation_in: [MutationType!]
  updatedFields_contains: String
  updatedFields_contains_every: [String!]
  updatedFields_contains_some: [String!]
  node: SpaceWhereInput
  AND: [SpaceSubscriptionWhereInput!]
  OR: [SpaceSubscriptionWhereInput!]
  NOT: [SpaceSubscriptionWhereInput!]
}

input SpaceUpdateInput {
  owner: UserUpdateOneRequiredInput
  name: String
  description: String
  bits: BitUpdateManyInput
}

input SpaceWhereInput {
  id: ID
  id_not: ID
  id_in: [ID!]
  id_not_in: [ID!]
  id_lt: ID
  id_lte: ID
  id_gt: ID
  id_gte: ID
  id_contains: ID
  id_not_contains: ID
  id_starts_with: ID
  id_not_starts_with: ID
  id_ends_with: ID
  id_not_ends_with: ID
  owner: UserWhereInput
  name: String
  name_not: String
  name_in: [String!]
  name_not_in: [String!]
  name_lt: String
  name_lte: String
  name_gt: String
  name_gte: String
  name_contains: String
  name_not_contains: String
  name_starts_with: String
  name_not_starts_with: String
  name_ends_with: String
  name_not_ends_with: String
  description: String
  description_not: String
  description_in: [String!]
  description_not_in: [String!]
  description_lt: String
  description_lte: String
  description_gt: String
  description_gte: String
  description_contains: String
  description_not_contains: String
  description_starts_with: String
  description_not_starts_with: String
  description_ends_with: String
  description_not_ends_with: String
  bits_every: BitWhereInput
  bits_some: BitWhereInput
  bits_none: BitWhereInput
  AND: [SpaceWhereInput!]
  OR: [SpaceWhereInput!]
  NOT: [SpaceWhereInput!]
}

input SpaceWhereUniqueInput {
  id: ID
  name: String
}

type Subscription {
  bit(where: BitSubscriptionWhereInput): BitSubscriptionPayload
  space(where: SpaceSubscriptionWhereInput): SpaceSubscriptionPayload
  user(where: UserSubscriptionWhereInput): UserSubscriptionPayload
}

type User {
  id: ID!
  email: String!
  password: String!
}

type UserConnection {
  pageInfo: PageInfo!
  edges: [UserEdge]!
  aggregate: AggregateUser!
}

input UserCreateInput {
  email: String!
  password: String!
}

input UserCreateOneInput {
  create: UserCreateInput
  connect: UserWhereUniqueInput
}

type UserEdge {
  node: User!
  cursor: String!
}

enum UserOrderByInput {
  id_ASC
  id_DESC
  email_ASC
  email_DESC
  password_ASC
  password_DESC
  createdAt_ASC
  createdAt_DESC
  updatedAt_ASC
  updatedAt_DESC
}

type UserPreviousValues {
  id: ID!
  email: String!
  password: String!
}

type UserSubscriptionPayload {
  mutation: MutationType!
  node: User
  updatedFields: [String!]
  previousValues: UserPreviousValues
}

input UserSubscriptionWhereInput {
  mutation_in: [MutationType!]
  updatedFields_contains: String
  updatedFields_contains_every: [String!]
  updatedFields_contains_some: [String!]
  node: UserWhereInput
  AND: [UserSubscriptionWhereInput!]
  OR: [UserSubscriptionWhereInput!]
  NOT: [UserSubscriptionWhereInput!]
}

input UserUpdateDataInput {
  email: String
  password: String
}

input UserUpdateInput {
  email: String
  password: String
}

input UserUpdateOneRequiredInput {
  create: UserCreateInput
  update: UserUpdateDataInput
  upsert: UserUpsertNestedInput
  connect: UserWhereUniqueInput
}

input UserUpsertNestedInput {
  update: UserUpdateDataInput!
  create: UserCreateInput!
}

input UserWhereInput {
  id: ID
  id_not: ID
  id_in: [ID!]
  id_not_in: [ID!]
  id_lt: ID
  id_lte: ID
  id_gt: ID
  id_gte: ID
  id_contains: ID
  id_not_contains: ID
  id_starts_with: ID
  id_not_starts_with: ID
  id_ends_with: ID
  id_not_ends_with: ID
  email: String
  email_not: String
  email_in: [String!]
  email_not_in: [String!]
  email_lt: String
  email_lte: String
  email_gt: String
  email_gte: String
  email_contains: String
  email_not_contains: String
  email_starts_with: String
  email_not_starts_with: String
  email_ends_with: String
  email_not_ends_with: String
  password: String
  password_not: String
  password_in: [String!]
  password_not_in: [String!]
  password_lt: String
  password_lte: String
  password_gt: String
  password_gte: String
  password_contains: String
  password_not_contains: String
  password_starts_with: String
  password_not_starts_with: String
  password_ends_with: String
  password_not_ends_with: String
  AND: [UserWhereInput!]
  OR: [UserWhereInput!]
  NOT: [UserWhereInput!]
}

input UserWhereUniqueInput {
  id: ID
  email: String
}
`
}
