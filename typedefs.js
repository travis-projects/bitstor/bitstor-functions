module.exports = `
  scalar Json

  type User {
    id: ID!
    email: String!
  }

  type AuthPayload {
    user: User!
    token: String!
  }

  type Space {
    id: ID!
    owner: User!
    name: String
    description: String
    bits: [Bit!]!
  }

  type Bit {
    id: ID!
    owner: User!
    name: String
    file: String
    version: Float
  }

  type Query {
    spaces: [Space!]!
    bits: [Bit!]!
  }

  type Mutation {
    signup(email: String!, password: String!): AuthPayload!
    login(email: String!, password: String!): AuthPayload!
    createSpace(name: String!, description: String): Space
    createBit(name: String!, file: String!, version: Float!): Bit
    createSignedUrl: Json!
    createDownloadAuth: Json!
  }
`
