const { prisma } = require('../prisma')

module.exports = {
  spaces: (parent, args, ctx, info) => {
    const userId = ctx.getUserId(ctx)
    return prisma.spaces({ where: { owner: { id: userId } } })
  },
  bits: (parent, args, ctx, info) => {
    const userId = ctx.getUserId(ctx)
    return prisma.bits({ where: { owner: { id: userId } } })
  }
}
