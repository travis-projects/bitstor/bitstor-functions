const { prisma } = require('../prisma')
const bcrypt = require('bcryptjs')
const jwt = require('jsonwebtoken')
const { getUploadUrl, getDownloadAuth } = require('../b2')

module.exports = {
  signup: async (parent, { email, password }, ctx, info) => {
    const hashedPassword = await bcrypt.hashSync(password, 10)
    const user = await prisma.createUser({ email, password: hashedPassword })
    const token = await jwt.sign({ userId: user.id }, process.env.APP_SECRET)
    return { user, token }
  },
  login: async (parent, { email, password }, ctx, info) => {
    try {
      const user = await prisma.user({ email })
      if (!user) throw new Error()
      const valid = await bcrypt.compare(password, user.password)
      if (!valid) throw new Error()
      const token = await jwt.sign({ userId: user.id }, process.env.APP_SECRET)
      return { token, user }
    } catch (error) {
      throw new Error(`Invalid user or password`)
    }
  },
  createSpace: async (parent, { name, description }, ctx, info) => {
    const userId = await ctx.getUserId(ctx)
    try {
      const space = await prisma.createSpace({ name, description, owner: { connect: { id: userId } } })
      return space
    } catch (error) {
      throw new Error(error.message)
    }
  },
  createBit: async (parent, { name, file, version }, ctx, info) => {
    const userId = await ctx.getUserId(ctx)
    try {
      const bit = await prisma.createBit({ name, file, version, owner: { connect: { id: userId } } })
      return bit
    } catch (error) {
      throw new Error(error.message)
    }
  },
  createSignedUrl: async (parent, args, ctx, info) => {
    await ctx.getUserId(ctx)
    try {
      return getUploadUrl()
    } catch (error) {
      throw new Error(error.message)
    }
  },
  createDownloadAuth: async (parent, args, ctx, info) => {
    const userId = await ctx.getUserId(ctx)
    try {
      return getDownloadAuth(userId)
    } catch (error) {
      throw new Error(error.message)
    }
  }
}
